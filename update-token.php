<?php
$dir = __DIR__ . '/assets/';
$files = array_diff(scandir($dir), array('..', '.', 'kytscha.css'));

function generateKey()
{
	$length = 64;
	$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$%^&*!<>~:;{}-+';
	$string = '';

	for ($iLoop = 0; $iLoop < $length; $iLoop++) {
	    $string .= $chars[rand(0, strlen($chars) - 1)];
	}

	return $string;
}

foreach ($files as $file) {
	/*
	 * Get the ASCII.
	 */
	 $contents = file_get_contents($dir . $file);
	 echo "Processing file " . $dir . $file . "\n";

	/*
	 * Split the contents on the keyword.
	 */
	 $splits = explode('|KYTSCHA|', $contents);

	/*
	 * Loop through the splits.
	 */
	foreach ($splits as $split) {
		if (empty($split)) {
			continue;
		}

		/*
		 * Separate the words from the art.
		 */
		$ascii = explode('|END|', $split);

		/*
		 * If we either have no words or art, just move on.
		 */
		if (empty($ascii[0]) || empty($ascii[1])) {
			continue;
		}

		/*
		 * Get the token.
		 */
		$tokens = explode('|', $ascii[0]);
		$token = $tokens[0];

		$contents = str_replace($token, generateKey(), $contents);
	}

	/*
	 * Save the new data.
	 */
	 try {
		 echo "Writing to file " . $dir . $file . "\n";
		 file_put_contents($dir . $file, $contents);
		 echo "Write to file " . $dir . $file . " complete\n";
	 } catch (\Exception $err) {
		 echo 'Error, ' . $err->getMessage();
	 }
}

echo "done\n";
