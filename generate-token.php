<?php
$length = 64;
$chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ$%^&*!<>~:;{}-+';
$string = '';

for ($iLoop = 0; $iLoop < $length; $iLoop++) {
    $string .= $chars[rand(0, strlen($chars) - 1)];
}

echo "\n" . $string . "\n\n";
