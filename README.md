# Project has been moved!
https://ironet.kytschi.com/kytschi/kytscha

# Kytschi's Kytscha

Kytscha is an ASCII based captcha package for PHP projects.

## WARNING!

This was made for fun and is by no means secure enough for really important systems.

## Installation

Include the repository to your composer.json

```
"repositories": [
    {
        "type":"git",
        "url": "https://bitbucket.org/its-kytschi/kytscha.git"
    }
],
```

## Changing the look

You can modify the look via the CSS. Kytscha makes use of a CSS class name .kytscha. The default is as follows.

```
.kytscha {
    background-color: #f45454;
    color: #fff;
    font-weight: bold;
    padding: 20px;
    font-size: 8pt;
}
```

## Regenerating the word tokens

For security reason it is a good idea to regenerate the word tokens that are used to help match the user's input with that of the ASCII art.

```
php -f update-tokens.php
```

## Creating your own files

If you want to create your own ASCII art files just add a text file in the `assets` folder and follow the example input.

```
|KYTSCHA|mWQyq$UGUiR8~2~!}}aZGVS:L{LrDSPDwO$Qe49NSvQzu-1u8>E%*B+fmkzyvV2t|bat|END|
   /\                 /\
  / \'._   (\_/)   _.'/ \
 /_.''._'--('.')--'_.''._\
 | \_ / `;=/ " \=;` \ _/ |
  \/ `\__|`\___/`|__/`  \/
jgs`      \(/|\)/       `
           " ` "
|KYTSCHA|
```

The token in the example `mWQyq$UGUiR8~2~!}}aZGVS:L{LrDSPDwO$Qe49NSvQzu-1u8>E%*B+fmkzyvV2t` can be generated using the following command.

```
php -f generate-token.php
```

## Thanks

Big thanks to https://www.asciiart.eu for the ASCII art and credit goes to the artists.
