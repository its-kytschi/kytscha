<?php

/**
 * Kytscha - just another captcha made by Kytschi
 *
 * @package     Kytschi\Kytscha\Controllers\Kytscha
 * @author      Kytschi (dev@kytschi.com)
 * @copyright   2022 Kytschi (kytschi.com)
 * @link        https://kytschi.com
 * @version     0.0.2
 */
namespace Kytschi\Kytscha\Controllers;

class Kytscha
{
    /**
     * Where all the ASCII will be stored.
     * @var array $ascii
     */
    private static $ascii = [];
	private static $tokens = [];
	private static $words = [];

    /**
     * Generate the captcha.
     *
     * @return string $html
     */
    public static function generate(): string
    {
        /*
         * Lets load the ASCII.
         */
        self::readFiles();

        /*
         * Get the array keys.
         */
        $keys = array_keys(self::$ascii);
		if (count($keys) <= 0) {
			throw new \Exception('Problem with Kytscha');
		}

        /*
         * Based on the array keys, pick one at random.
         */
		mt_srand(mt_rand(100000, 999999));
		$key = $keys[mt_rand(0, count($keys) - 1)];

        /*
         * Using that random key, select an entry from the array.
         */
        $ascii = self::$ascii[$key];

        /*
         * Load the CSS.
         */
        $html = '<style>';
        $html .= file_get_contents(__DIR__ . '/../../assets/kytscha.css');
        $html .= '</style>';

        /*
         * Build the ASCII captcha
         */
        $html .= '<pre id="kytscha-' . time() . '" class="kytscha">' . self::$ascii[$key] . '</pre>';
        $html .= '<input name="kytscha" type="text" required="required" class="form-control" ';
        $html .= 'placeholder="What do you see in the above picture?" />';
		$html .= '<input name="_KYTSCHA" type="hidden" value="' . self::$tokens[$key] .'"/>';

        /*
         * Return the html.
         */
        return $html;
    }

    /**
     * Read the files for the ASCII.
     *
     * @return void
     */
    private static function readFiles()
    {
		/*
		 * Where the ASCII text files are located.
		 */
		$dir = __DIR__ . '/../../assets/';

		/*
		 * Find the ASCII files.
		 */
		$files = array_diff(scandir($dir), array('..', '.', 'kytscha.css'));

        /*
         * Loop through the available ASCII files.
         */
        foreach ($files as $file) {
            /*
             * Define the full file with path.
             */
            $file =  $dir . $file;

            /*
             * Just double check that the file is there.
             */
            if (!file_exists($file)) {
				echo 'no file';
                continue;
            }

            /*
             * Load the file contents.
             */
            $contents = file_get_contents($file);

            /*
             * Split the contents on the keyword.
             */
            $splits = explode('|KYTSCHA|', $contents);

            /*
             * Loop through the splits.
             */
            foreach ($splits as $split) {
				if (empty($split)) {
					continue;
				}

                /*
                 * Separate the words from the art.
                 */
                $ascii = explode('|END|', $split);

                /*
                 * If we either have no words or art, just move on.
                 */
                if (empty($ascii[0]) || empty($ascii[1])) {
                    continue;
                }

				/*
				 * Save the token.
				 */
				$tokens = explode('|', $ascii[0]);
				$token = $tokens[0];
				unset($tokens[0]);

				/*
				 * Encryt the token
				 */
				mt_srand(mt_rand(100000, 999999));
				self::$tokens[$token] = @openssl_encrypt(
					'KYTSCHA=' . $token . '=' . mt_rand(0, 99999999),
					'aes128',
					$token,
					$options=0,
					date('Y-m-d H:i')
				);

                /*
                 * Save the words and the art.
                 */
                self::$ascii[$token] = $ascii[1];
				self::$words[$token] = implode('|', $tokens);
			}
        }
    }

    /**
     * Validating the captcha.
     *
     * @return bool
     */
    public static function validate(): bool
    {
        /*
         * If the kytscha value is not set its invalid.
         */
		if (empty($_REQUEST['kytscha']) || empty($_REQUEST['_KYTSCHA'])) {
			return false;
		}

		/*
		 * Lets load the ASCII.
		 */
		self::readFiles();

		/*
		 * Decrypt the token and check for valid word.
		 */
		foreach (self::$tokens as $key => $hash) {
			$token = openssl_decrypt(
				$_REQUEST['_KYTSCHA'],
				'aes128',
				$key,
				$options=0,
				date('Y-m-d H:i')
			);
			
			if (substr($token, 0, 8) != 'KYTSCHA=') {
				continue;
			}

			/*
			 * Split the saved kytscha in case there is multiple answers.
			 * Why mulitple answers? It could be different spellings of the item in the picture.
			*/
			$splits = explode('=', $token);
			if (empty(self::$words[$splits[1]])) {
				return false;
			}
			$splits = explode(', ', strtolower(self::$words[$splits[1]]));
			return in_array(strtolower($_REQUEST['kytscha']), $splits);
		}

		/*
		 * Invalid.
		 */
		return false;
    }
}
